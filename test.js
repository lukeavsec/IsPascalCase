import Assert       from '/p/Assert/index.js'
import IsBoolean    from '/p/IsBoolean/index.js'
import IsPascalCase from './index.js'
import IsString     from '/p/IsString/index.js'
import Test         from '/p/Muster/index.js'



Object.entries({

	'A':         true,
	'A1':        true,
	'Foo':       true,
	'FooBar':    true,
	'Foobar':    true,
	'FooBar3':   true,
	'AbCdE':     true,
	'AbCdEf':    true,
	'FooBar435': true,
	'FooBarBaz': true,
	'FooBarB':   true,
	'FooBarB3':  true,

	'a':            false,
	'7':            false,
	'':             false,
	'foo':          false,
	'foo1':         false,
	'fooBar1':      false,
	'Foo Bar':      false,
	'fooBar':       false,
	'foo_bar':      false,
	'foo-bar':      false,
	'FOOBAR':       false,
	'FooBar3A':     false,
	'FooBarB3p0':   false,
	'aBcDeF':       false,
	'1FooBar':      false,
	'BigHTTPError': false,

})
.forEach(([string, boolean]) => {
	Test(`IsPascalCase("${string}") === ${boolean}`, () => {

		Assert(IsString(string))
		Assert(IsBoolean(boolean))

		Assert(IsPascalCase(string) === boolean)

	})
})
